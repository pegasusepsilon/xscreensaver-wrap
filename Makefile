WARNINGS = -Wall -Werror -Wextra -Wshadow -Wconversion -Winline -Wformat=2 -Wno-comment
OBJECTS = xscreensaver-wrap.o
LIBS = -lxcb

ifeq ($(shell $(CC) --version | awk 'NR == 1 { print $$1 }'),clang)
FLAGS = -fsanitize=shift
else # assume GCC
FLAGS = -fmax-errors=5
WARNINGS += -Wformat-overflow=2 -Wformat-truncation=2
endif

CFLAGS = -Os -I.. -ansi -pedantic -std=c99 $(FLAGS) $(WARNINGS)

default: xscreensaver-wrap

xscreensaver-wrap: $(OBJECTS)
	$(CC) $^ $(LIBS) -o $@
	strip -s xscreensaver-wrap
	strip -R .note.gnu.build-id -R .note.ABI-tag -R .gnu.hash -R .gnu.version \
		-R .eh_frame_hdr -R .eh_frame -R .init_array -R .fini_array \
		-R .got -R .data -R .comment -R .symtab -R .strtab xscreensaver-wrap
	# -R .bss crashes with CC="zig cc"

clean:
	rm *.o xscreensaver-wrap || :
