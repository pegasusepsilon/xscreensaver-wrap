/* xscreensaver-wrap.c - run an xscreensaver as wallpaper under a compositor
** (C)opyright 2023 "Pegasus Epsilon" <pegasus@pimpninjas.org>
** Distribute Unmodified - https://pegasus.pimpninjas.org/license
*/

// path to xscreensavers - glmatrix, glslideshow, etc
#define XSSPATH "/usr/lib/xscreensaver"

#define _XOPEN_SOURCE 500 // vfork()
#define _GNU_SOURCE 	// execvpe()
#include <unistd.h> 	// vfork(), fork(), execvpe(), exit(), _exit(), sleep()
#include <string.h> 	// strncmp(), strlen()
#include <stdio.h>  	// printf(), snprintf()
#include <stdlib.h> 	// free()
#include <signal.h> 	// kill(), sigaction()
#include <sys/wait.h>	// wait()
#include <errno.h>  	// errno. duh.
#include <stdbool.h>	// true, false
#include <xcb/xcb.h>	// xcb_*
// this should be standard...
#define XCB_REQUEST_ERROR 0

xcb_connection_t *xcb_connection_p;
xcb_screen_t *xcb_screen_p;

xcb_atom_t get_atom (char *atom) {
	xcb_intern_atom_reply_t *reply = xcb_intern_atom_reply(
		xcb_connection_p,
		xcb_intern_atom(xcb_connection_p, false, (uint16_t)strlen(atom), atom),
		NULL
	);
	xcb_atom_t result = reply ? reply->atom : 0;
	free(reply);
	return result;
}

xcb_window_t wrapper (xcb_window_t xcb_window_id) {
	xcb_screen_p = xcb_setup_roots_iterator(
		xcb_get_setup(xcb_connection_p)).data;
	xcb_depth_t *xcb_depth_p = xcb_screen_allowed_depths_iterator(
		xcb_screen_p).data;
	xcb_visualid_t xcb_visual_id = xcb_depth_visuals_iterator(
		xcb_depth_p).data->visual_id;

	xcb_create_window(
		xcb_connection_p, xcb_depth_p->depth, // connection, bit-depth
		xcb_window_id, xcb_screen_p->root, // id, parent
		0, 0, xcb_screen_p->width_in_pixels, // x, y, width
		xcb_screen_p->height_in_pixels, 0, // height, border
		XCB_WINDOW_CLASS_INPUT_OUTPUT, // INPUT_ONLY cannot be shown, so...
		xcb_visual_id, // still don't understand what a visual is...
		XCB_CW_BACK_PIXEL | XCB_CW_OVERRIDE_REDIRECT | XCB_CW_EVENT_MASK,
		(uint32_t []){
			xcb_screen_p->black_pixel, true, XCB_EVENT_MASK_RESIZE_REDIRECT
		}
	);

	xcb_configure_window(xcb_connection_p, xcb_window_id,
		XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y |
		XCB_CONFIG_WINDOW_WIDTH | XCB_CONFIG_WINDOW_HEIGHT |
		XCB_CONFIG_WINDOW_STACK_MODE, (uint32_t []){
			0, 0, // x, y
			xcb_screen_p->width_in_pixels, // width
			xcb_screen_p->height_in_pixels, // height
			XCB_STACK_MODE_BELOW // desktop
		}
	);

	xcb_change_property(xcb_connection_p, XCB_PROP_MODE_REPLACE,
		xcb_window_id, get_atom("_NET_WM_WINDOW_TYPE"), XCB_ATOM_ATOM,
		32, 1, (xcb_atom_t []){ get_atom("_NET_WM_WINDOW_TYPE_DESKTOP") });

	xcb_map_window(xcb_connection_p, xcb_window_id);

	return xcb_window_id;
}

void usage (char *myself) {
	printf("This is https://gitlab.com/pegasusepsilon/xscreensaver-wrap\n");
	printf("Usage: %s [screensaver] <args>\n", myself);
	printf("eg: %s glslideshow --zoom 95 --pan 10", myself);
	printf(" --duration 10 --clip\n");
	exit(0);
}

pid_t screensaver;
pid_t fake_root;
void cleanup (void) {
	if (screensaver) kill(screensaver, SIGTERM);
	if (fake_root) kill(fake_root, SIGTERM);
}

void handle_signal (int signal) {
	cleanup();
	exit(signal);
}

xcb_window_t fake_root_window (char *argv0, size_t space) {
	xcb_connection_p = xcb_connect(NULL, NULL);
	xcb_window_t xcb_window_id = xcb_generate_id(xcb_connection_p);

	if ((fake_root = fork())) return xcb_window_id;

	wrapper(xcb_window_id);
	strncpy(argv0, "xscreensaver-wrap/fake-root", space);
	while (xcb_flush(xcb_connection_p)) {
		free(xcb_wait_for_event(xcb_connection_p));
		xcb_configure_window(xcb_connection_p, xcb_window_id,
			XCB_CONFIG_WINDOW_X | XCB_CONFIG_WINDOW_Y, (uint32_t []){ 0, 0 }
		);
		xcb_circulate_window(xcb_connection_p, XCB_CIRCULATE_LOWER_HIGHEST,
			xcb_window_id);
	}

	return 0;
}

size_t get_argv_space (char **argv) {
	size_t space = 0;
	for (int i = 0; argv[i]; i++)
		space += strlen(argv[i]) + 1;
	return space;
}

char **copy_argv (size_t argc, char **argv) {
	char **copy = calloc(argc + 1, sizeof(char *));
	for (size_t i = 0; i < argc && argv[i]; i++)
		copy[i] = strdup(argv[i]);
	return copy;
}

void hook_signals (void) {
	struct sigaction act = { .sa_handler = &handle_signal };
	sigemptyset(&act.sa_mask);
	sigaction(SIGINT, &act, NULL);
	sigaction(SIGTERM, &act, NULL);
}

int main (int argc, char **argv, char **envp) {

	if (argc < 2) usage(argv[0]);

	hook_signals();

	// put screensavers into the path
	int path = 0;
	char *pathstr = "PATH=";
	while (envp[path] && strncmp(envp[path], pathstr, strlen(pathstr)))
		path++;
	if (envp[path]) {
		size_t len = strlen(envp[path]);
		char *dest = malloc(len + strlen(":" XSSPATH) + 1);
		envp[path] = strcat(strcpy(dest, envp[path]), ":" XSSPATH);
	}

	// make fake root window
	size_t argv_size = get_argv_space(argv);
	xcb_window_t xcb_window_id = fake_root_window(argv[0], argv_size);

	// tell the xscreensaver to draw on our window
	char **argv_copy = copy_argv((size_t)argc + 1, &argv[1]);
	argv_copy[argc - 1] = "--window-id";
	{
		size_t len = (size_t)snprintf(NULL, 0, "0x%08x", xcb_window_id) + 1;
		argv_copy[argc] = calloc(1, len);
		snprintf(argv_copy[argc], len, "0x%08x", xcb_window_id);
	};

	for (;;) { // ever
		// vfork() is for when you will immediately exec*()
		// vfork() is lighter and faster than fork()
		// calling anything other than exec*() or _exit() is UB
		if (!(screensaver = vfork())) {
			execvpe(argv_copy[0], argv_copy, envp);
			// can't perror() here, that's UB.
			// pass errno back to parent instead.
			_exit(errno); // exit() is UB, must use _exit()
		}

		strncpy(argv[0], "xscreensaver-wrap/supervisor", argv_size);

		int status;
		wait(&status);
		if (WIFEXITED(status)) { // look for child's errno
			errno = WEXITSTATUS(status);
			perror("could not execute requested screensaver");
			for (int i = 0; i < argc - 1; i++) free(argv_copy[i]);
			free(argv_copy);
			free(envp[path]);
			cleanup();
			exit(errno);
		}

		fprintf(stderr, "%s: %s died, sleeping 1s...\n", argv[0], argv_copy[0]);

		sleep(1);
	};
}
